import 'package:prepareNcare/screens/settings.dart';
import 'package:prepareNcare/screens/analytics.dart';
import 'package:flutter/material.dart';
import 'package:prepareNcare/screens/stock.dart';
import 'package:prepareNcare/screens/startGroceryList.dart';
import 'package:prepareNcare/screens/planer.dart';
import 'package:prepareNcare/screens/settings.dart';
import 'package:prepareNcare/screens/home.dart';


void main() => runApp(MyApp());


const String settings = "Einstellungen";
const String grocery = "Einkaufsliste";
const String planer = "Planer";
const String stock = "Vorrat";
const String analytics = "Analytics";
const String title = "Leon Test";

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: title,
      home: new MyHomePage(title: title),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int selectedPage = 0;
  int currentIndex = 0;
  String _title;

  List<Widget> _pages;
  Widget _settings;
  Widget _stock;
  Widget _grocery;
  Widget _planer;
  Widget _analytics;

  int _currentIndex;
  Widget _currentPage;

  @override
  void initState() {
    super.initState();

    _settings = Settings();
    _stock = Stock();
    _grocery = Grocery();
    _planer = Planer();
    _analytics = Analytics();

    _title = "Vorrat";

    _pages = [_analytics, _grocery, _stock, _planer, _settings];

    _currentIndex = 2;
    _currentPage = _stock;
  }

  void changeTab(int index){
    setState(() {
      _currentIndex = index;
      _currentPage = _pages[index];
      switch(index) {
        case 4: { _title = 'Einstellungen'; }
        break;
        case 2: { _title = 'Vorrat'; }
        break;
        case 1: { _title = 'Einkaufsliste'; }
        break;
        case 3: { _title = 'Planer'; }
        break;
        case 0: { _title = 'Analytics'; }
        break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Color.fromRGBO(43, 125, 50, 1),
        centerTitle: true,
        title: new Text(_title),
        actions: <Widget>[
          /*IconButton(
            icon: Icon(
              Icons.show_chart,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => Analytics()),
              );
            },
          ),*/
         /* IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()),
              );
            },
          )*/
        ],
      ),
      body: _currentPage,
      bottomNavigationBar: BottomNavigationBar(
        onTap: (index) => changeTab(index),
        currentIndex: _currentIndex,
          backgroundColor: Colors.white,
        //  orange backgroundColor: Color.fromRGBO(233, 83, 31, 1),
         // dk Grün von Logo -- Color.fromRGBO(43, 125, 50, 1),
        selectedItemColor: Color.fromRGBO(43, 125, 50, 1),
        unselectedItemColor: Colors.black,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.show_chart), title: Text(analytics, textScaleFactor:  0.75,), ),
          BottomNavigationBarItem(icon: Icon(Icons.add_shopping_cart), title: Text(grocery, textScaleFactor:  0.75,)),
          BottomNavigationBarItem(icon: Icon(Icons.kitchen), title: Text(stock, textScaleFactor:  0.75,)),
          BottomNavigationBarItem(icon: Icon(Icons.event), title: Text(planer, textScaleFactor:  0.75,)),
          BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text(settings, textScaleFactor:  0.75,)),

          //BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text(page5)),

        ]),
    );
  }
}


