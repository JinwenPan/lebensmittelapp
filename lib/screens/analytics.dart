import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'dart:math';
import 'package:charts_flutter/flutter.dart' as charts;

class Analytics extends StatefulWidget {
  @override
  _AnalyticsState createState() => _AnalyticsState();
}

class ClicksPerYear {
  final String year;
  final int clicks;
  final charts.Color color;

  ClicksPerYear(this.year, this.clicks, Color color)
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}

class _AnalyticsState extends State<Analytics> {
  int itemsUsed = Strings.paprikaused +
      Strings.butterused +
      Strings.milkused +
      Strings.cheeseused +
      Strings.sausageused +
      Strings.applejuiceused +
      Strings.iceused +
      Strings.icecreamchocoused +
      Strings.icecreamstrawused +
      Strings.toiletpaperused +
      Strings.tomatoesused;
  int itemsWasted = Strings.paprikatrash +
      Strings.buttertrash +
      Strings.milktrash +
      Strings.cheesetrash +
      Strings.sausagetrash +
      Strings.applejuicetrash +
      Strings.icetrash +
      Strings.icecreamchocotrash +
      Strings.icecreamstrawtrash +
      Strings.toiletpapertrash +
      Strings.tomatoestrash;
  int community = 4;
  double verschwendung = 1.0;
  String zeitraumString = "Dieser Monat";

  @override
  Widget build(BuildContext context) {
    double itemsWastedDouble = itemsWasted.toDouble();
    verschwendung = (Strings.euro * itemsWastedDouble);

    var data = [
      //    ClicksPerYear('2016', 12, Colors.red),
      ClicksPerYear('Verbraucht', itemsUsed, Color.fromRGBO(43, 125, 50, 1),),
      ClicksPerYear('Verschwendet', itemsWasted, Color.fromRGBO(233, 83, 31, 1)),
    ];

    var data2 = [
      //    ClicksPerYear('2016', 12, Colors.red),
      ClicksPerYear('Du', itemsWasted, Color.fromRGBO(43, 125, 50, 1),),
      ClicksPerYear('Community', community, Color.fromRGBO(233, 83, 31, 1)),
    ];

    var series = [
      charts.Series(
        domainFn: (ClicksPerYear clickData, _) => clickData.year,
        measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
        colorFn: (ClicksPerYear clickData, _) => clickData.color,
        id: 'Clicks',
        data: data,
      ),
    ];

    var series2 = [
      charts.Series(
        domainFn: (ClicksPerYear clickData, _) => clickData.year,
        measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
        colorFn: (ClicksPerYear clickData, _) => clickData.color,
        id: 'Clicks',
        data: data2,
      ),
    ];

    var chart = charts.BarChart(
      series,
      animate: true,
    );

    var chart2 = charts.BarChart(
      series2,
      animate: true,
    );

    var chartWidget = Padding(
      padding: EdgeInsets.all(5.0),
      child: SizedBox(
        height: MediaQuery.of(context).size.height / 6,
        child: chart,
      ),
    );

    var chartCompetitor = Padding(
      padding: EdgeInsets.all(5.0),
      child: SizedBox(
        height: MediaQuery.of(context).size.height / 6,
        child: chart2,
      ),
    );

    var textWidget = Padding(
      padding: EdgeInsets.all(5.0),
      child: SizedBox(
        height: 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Verschwendung insgesamt: ${verschwendung.toStringAsFixed(2)} €",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(""),
            Text("Deine Verschwendungsübersicht:"),
          ],
        ),
      ),
    );

    var zeitraumWidget = Padding(
      padding: EdgeInsets.all(5.0),
      child: SizedBox(
        height: 25,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Text("Zeitraum     "),
            ),
            Container(
              child: DropdownButton<String>(
                value: zeitraumString,
                icon: Icon(Icons.arrow_drop_down),
                iconSize: 20,
                onChanged: (String newValue) {
                  setState(() {
                    zeitraumString = newValue;
                  });
                },
                items: <String>[
                  'Dieser Monat',
                  'Letzter Monat',
                  'Diese Woche',
                  'Alle Daten'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            )
          ],
        ),
      ),
    );

    var textWidget2 = Padding(
      padding: EdgeInsets.all(5.0),
      child: SizedBox(
        height: 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Du gegen Deine Community:"),
          ],
        ),
      ),
    );

    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              textWidget,
              zeitraumWidget,
              chartWidget,
              textWidget2,
              zeitraumWidget,
              chartCompetitor
            ],
          ),
        ),
      ),
    );
  }
}
