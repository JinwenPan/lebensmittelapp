import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'package:prepareNcare/screens/scan.dart';

class AddStockItems extends StatefulWidget {
  @override
  _AddStockItemsState createState() => _AddStockItemsState();
}

class _AddStockItemsState extends State<AddStockItems> {

  String productString = "Paprika";
  int mengeString = 1;
  String kategorieString = "Gemüse";
  String lagerortString = "Kühlschrank";
  String preisString = "1€";
  String info = "";
  bool flag = false;

  @override
  Widget build(BuildContext context) {
    return _buildAddStockItemsScreen();
  }

  Scaffold _buildAddStockItemsScreen() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(43, 125, 50, 1),
        title: Text("Lebensmittel Hinzufügen"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            
/*           Container(
              child:new Image.asset('lib/assets/leibao.jpg'),
              width:300.0,
              height:200.0,
            ),
*/



            Container(
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Scan()),
                  );
                },
                child: Text("Kassenbon Scannen"),
              ),
            ),




            Container(
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Scan()),
                  );
                },
                child: Text("Barcode Scannen"),
              ),
            ),



              Container(

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text("Produkt     "),
                  ),
                  Container(
                    child: DropdownButton<String>(
                      value: productString,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 20,
                      onChanged: (String newValue) {
                        setState(() {
                          productString = newValue;
                        });
                      },
                      items: <String>['Apfelsaft', 'Paprika', 'Butter', 'Käse', 'Eiswürfel', 'Schokoeis', 'Erdbeereis', 'Milch', 'Salamie', 'Toilettenpapier', "Tomaten"]
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            ),


            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text("Menge     "),
                  ),
                  Container(
                    child: DropdownButton<int>(
                      value: mengeString,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 20,
                      onChanged: (int newValue) {
                        setState(() {
                          mengeString = newValue;
                        });
                      },
                      items: <int>[1, 2, 3, 4]
                          .map<DropdownMenuItem<int>>((int value) {
                        return DropdownMenuItem<int>(
                          value: value,
                          child: Text("$value"),
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            ),

/*
            Container(child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text("Kategorie     "),
                ),
                Container(
                  child: DropdownButton<String>(
                    value: kategorieString,
                    icon: Icon(Icons.arrow_drop_down),
                    iconSize: 20,
                    onChanged: (String newValue) {
                      setState(() {
                        kategorieString = newValue;
                      });
                    },
                    items: <String>['Gemüse', 'Obst', 'Eis', 'Hygiene']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),),


            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text("Lagerort     "),
                  ),
                  Container(
                    child: DropdownButton<String>(
                      value: lagerortString,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 20,
                      onChanged: (String newValue) {
                        setState(() {
                          lagerortString = newValue;
                        });
                      },
                      items: <String>['Kühlschrank', 'Vorratsschrank', 'Tiefkühlfach']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            ),

*/
            Container(child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text("Preis     "),
                ),
                Container(
                  child: DropdownButton<String>(
                    value: preisString,
                    icon: Icon(Icons.arrow_drop_down),
                    iconSize: 20,
                    onChanged: (String newValue) {
                      setState(() {
                        preisString = newValue;
                      });
                    },
                    items: <String>['1€', '2€', '3€', '4€']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),),


      Container(
        child: RaisedButton(
          onPressed: () {
            switch(productString){
              case "Paprika": setState(() {
                Strings.paprika = Strings.paprika + mengeString;
                info = "Informationen ueber Paprika";
                flag = false;
              }); break;
              case "Apfelsaft" : setState(() {
                Strings.applejuice = Strings.applejuice + mengeString;
                info = "Informationen ueber Apfelsaft";
              }); break;
              case "Butter" : setState(() {
                Strings.butter = Strings.butter + mengeString;
                info = "Informationen ueber Butter";
              }); break;
              case "Käse" : setState(() {
                Strings.cheese = Strings.cheese + mengeString;
                info = "Informationen ueber Käse";
                flag = true;
              }); break;
              case "Eiswürfel" : setState(() {
                Strings.ice = Strings.ice + mengeString;
                info = "Informationen ueber Eiswürfel";
              }); break;
              case "Schokoeis" : setState(() {
                Strings.icecreamchoco = Strings.icecreamchoco + mengeString;
                info = "Informationen ueber Schokoeis";
              }); break;
              case "Erdbeereis" : setState(() {
                Strings.icecreamstraw = Strings.icecreamstraw + mengeString;
                info = "Informationen ueber Erdbeereis";
              }); break;
              case "Milch" : setState(() {
                Strings.milk = Strings.milk + mengeString;
                info = "Informationen ueber Milch";
              }); break;
              case "Salamie" : setState(() {
                Strings.sausage = Strings.sausage + mengeString;
                info = "Informationen ueber Salamie";
              }); break;
              case "Toilettenpapier" : setState(() {
                Strings.toiletpaper = Strings.toiletpaper + mengeString;
                info = "Informationen ueber Toilettenpapier";
              }); break;
              case "Tomaten" : setState(() {
                Strings.tomatoes = Strings.tomatoes + mengeString;
                info = "Informationen ueber Tomaten";
              }); break;
            }
            Navigator.pop(context);
            if (flag == true){
            return showDialog(
              context: context,
              builder: (context) {
                 return AlertDialog(
            // Retrieve the text the user has entered by using the
            // TextEditingController.
                  content: Text(info),
                  );
                },
              );
            }



          },
          child: Text("Hinzufügen"),
        ),
      ),


          ],
        ),
      ),
    );
  }
}
