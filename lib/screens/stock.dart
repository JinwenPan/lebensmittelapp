import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'dart:math';
import 'package:prepareNcare/screens/addStockItems.dart';
import 'package:prepareNcare/screens/detailPage.dart';

class Stock extends StatefulWidget {
  Stock({Key key}) : super(key: key);

  @override
  _StockState createState() => _StockState();
}

class _StockState extends State<Stock> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                //Button Lebensmittel hinzufügen
                Container(
                  height: MediaQuery.of(context).size.height / 30,
                  margin: EdgeInsets.symmetric(vertical: 25.0),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddStockItems()),
                      ).then((value) => setState(() {}));
                    },
                    child: const Text('Lebensmittel Hinzufügen',
                        style: TextStyle(fontSize: 20)),
                  ),
                ),

                //Überschrift Kühlschrank
                Container(
                  child: Text(
                    "Kühlschrank",
                    textAlign: TextAlign.start,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),

                //Kühlschrank Elemente
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20.0),
                  height: 160,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      //Hier beginnt der Container für ein Element

                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Paprika:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.paprika}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Detail()),
                                  ).then((value) => setState(() {}));
                                },
                                child:
                                    Image.asset('lib/assets/bell-peppers.png'),
                              ),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.paprika > 0) {
                                            Strings.paprika--;
                                            Strings.paprikaused++;

                                            //Einkaufsliste anpassen
                                            for (var i = 0;
                                                i < Strings.itemsneed2.length;
                                                i++) {
                                              if (Strings.itemsneed2[i][0] ==
                                                  'Paprika') {
                                                Strings.itemsneed2[i][1] =
                                                    Strings.itemsneed2[i][1] +
                                                        1;
                                                Strings.itemsneed2[i][2] =
                                                    false;
                                                break;
                                              }
                                            }
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(() {
                                        if (Strings.paprika > 0) {
                                          Strings.paprika--;
                                          Strings.paprikatrash++;

                                          //Einkaufsliste anpassen
                                          for (var i = 0;
                                              i < Strings.itemsneed2.length;
                                              i++) {
                                            if (Strings.itemsneed2[i][0] ==
                                                'Paprika') {
                                              Strings.itemsneed2[i][1] =
                                                  Strings.itemsneed2[i][1] + 1;
                                              Strings.itemsneed2[i][2] = false;
                                              break;
                                            }
                                          }
                                        }
                                        ;
                                      });
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),
                      //Hier beginnt der Container für ein Element

                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Butter:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.butter}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/butter.png'),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.butter > 0) {
                                            Strings.butter--;
                                            Strings.butterused++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(() {
                                        if (Strings.butter > 0) {
                                          Strings.butter--;
                                          Strings.buttertrash++;
                                        }
                                        ;
                                      });
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),
                      //Hier beginnt der Container für ein Element

                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Käse:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.cheese}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/cheese.png'),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.cheese > 0) {
                                            Strings.cheese--;
                                            Strings.cheeseused++;

                                            //Einkaufsliste anpassen
                                            for (var i = 0;
                                                i < Strings.itemsneed2.length;
                                                i++) {
                                              if (Strings.itemsneed2[i][0] ==
                                                  'Käse') {
                                                Strings.itemsneed2[i][1] =
                                                    Strings.itemsneed2[i][1] +
                                                        1;
                                                Strings.itemsneed2[i][2] =
                                                    false;
                                                break;
                                              }
                                            }
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.cheese > 0) {
                                            Strings.cheese--;
                                            Strings.cheesetrash++;

                                            //Einkaufsliste anpassen
                                            for (var i = 0;
                                                i < Strings.itemsneed2.length;
                                                i++) {
                                              if (Strings.itemsneed2[i][0] ==
                                                  'Käse') {
                                                Strings.itemsneed2[i][1] =
                                                    Strings.itemsneed2[i][1] +
                                                        1;
                                                Strings.itemsneed2[i][2] =
                                                    false;
                                                break;
                                              }
                                            }
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),
                      //Hier beginnt der Container für ein Element

                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Milch:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.milk}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/milk.png'),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.milk > 0) {
                                            Strings.milk--;
                                            Strings.milkused++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.milk > 0) {
                                            Strings.milk--;
                                            Strings.milktrash++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),

                      //Hier beginnt der Container für ein Element

                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Salamie:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.sausage}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/sausage.png'),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.sausage > 0) {
                                            Strings.sausage--;
                                            Strings.sausageused++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.sausage > 0) {
                                            Strings.sausage--;
                                            Strings.sausagetrash++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                      ),
                    ],
                  ),
                ),

                //Überschrift Vorratsschrank
                Container(
                  child: Text(
                    "Vorratsschrank",
                    textAlign: TextAlign.start,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),

                //Vorratsschrank Elemente
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20.0),
                  height: 160,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Toilettenpapier:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.toiletpaper}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/toiletpaper.png'),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.toiletpaper > 0) {
                                            Strings.toiletpaper--;
                                            Strings.toiletpaperused++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.toiletpaper > 0) {
                                            Strings.toiletpaper--;
                                            Strings.toiletpapertrash++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),
                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Tomaten:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.tomatoes}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/tomatoes.png'),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.tomatoes > 0) {
                                            Strings.tomatoes--;
                                            Strings.tomatoesused++;

                                            //Einkaufsliste anpassen
                                            for (var i = 0;
                                                i < Strings.itemsneed2.length;
                                                i++) {
                                              if (Strings.itemsneed2[i][0] ==
                                                  'Tomaten') {
                                                Strings.itemsneed2[i][1] =
                                                    Strings.itemsneed2[i][1] +
                                                        1;
                                                Strings.itemsneed2[i][2] =
                                                    false;
                                                break;
                                              }
                                            }
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.tomatoes > 0) {
                                            Strings.tomatoes--;
                                            Strings.tomatoestrash++;

                                            //Einkaufsliste anpassen
                                            for (var i = 0;
                                                i < Strings.itemsneed2.length;
                                                i++) {
                                              if (Strings.itemsneed2[i][0] ==
                                                  'Tomaten') {
                                                Strings.itemsneed2[i][1] =
                                                    Strings.itemsneed2[i][1] +
                                                        1;
                                                Strings.itemsneed2[i][2] =
                                                    false;
                                                break;
                                              }
                                            }
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),
                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Apfelsaft:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.applejuice}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/applejuice.png'),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.applejuice > 0) {
                                            Strings.applejuice--;
                                            Strings.applejuiceused++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.applejuice > 0) {
                                            Strings.applejuice--;
                                            Strings.applejuicetrash++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),
                    ],
                  ),
                ),

                //Überschrift Tiefkühlschrank
                Container(
                  child: Text(
                    "Tiefkühlschrank",
                    textAlign: TextAlign.start,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),

                //Tiefkühlschrank Elemente
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20.0),
                  height: 160,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Schokoeis:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.icecreamchoco}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child:
                                  Image.asset('lib/assets/icecreamchoco.png'),
                              width: 50,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.icecreamchoco > 0) {
                                            Strings.icecreamchoco--;
                                            Strings.icecreamchocoused++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.icecreamchoco > 0) {
                                            Strings.icecreamchoco--;
                                            Strings.icecreamchocotrash++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),
                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Eiswürfel:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.ice}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/ice.png'),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.ice > 0) {
                                            Strings.ice--;
                                            Strings.iceused++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.ice > 0) {
                                            Strings.ice--;
                                            Strings.icetrash++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),
                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Erdbeereis:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.icecreamstraw}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child:
                                  Image.asset('lib/assets/icecreamstraw.png'),
                              width: 50.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.icecreamstraw > 0) {
                                            Strings.icecreamstraw--;
                                            Strings.icecreamstrawused++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "-",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(
                                        () {
                                          if (Strings.icecreamstraw > 0) {
                                            Strings.icecreamstraw--;
                                            Strings.icecreamstrawtrash++;
                                          }
                                          ;
                                        },
                                      );
                                    },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Icon(Icons.restore_from_trash),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),
                    ],
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
