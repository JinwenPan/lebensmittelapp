import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'dart:math';

class Seite4 extends StatefulWidget {
  //static const String routeName = Strings.routeSeite4;

  @override
  _Seite4State createState() => _Seite4State();
}

class _Seite4State extends State<Seite4> {



  @override
  Widget build(BuildContext context) {
    return _buildSeite4Screen();
  }

  Scaffold _buildSeite4Screen() {

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Seite 4'),
          ],
        ),
      ),
    );
  }
}
