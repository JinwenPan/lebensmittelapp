import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'dart:math';

class Detail extends StatefulWidget {
  //static const String routeName = Strings.routeSeite4;

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  String productString = "Paprika";
  int mengeString = 1;
  String kategorieString = "Gemüse";
  String lagerortString = "Kühlschrank";
  String mindestmengeString = "1";
  String einheitString = "1 Stk.";
  String preisString = "1€";
  String mhdString = "3 Tage";

  @override
  Widget build(BuildContext context) {
    return _buildDetailScreen();
  }

  Scaffold _buildDetailScreen() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(43, 125, 50, 1),
        title: Text("Detailsseite"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.all(5.0),
                child: Text(
                  ("Paprika"),
                ),
              ),
              Container(
                margin: EdgeInsets.all(5.0),
                child: Image.asset(
                  ('lib/assets/bell-peppers.png'),
                ),
                width: 50.0,
                height: 50,
              ),

              /*Container(

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text("Produkt     "),
                  ),
                  Container(
                    child: DropdownButton<String>(
                      value: productString,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 20,
                      onChanged: (String newValue) {
                        setState(() {
                          productString = newValue;
                        });
                      },
                      items: <String>['Apfelsaft', 'Paprika', 'Butter', 'Käse', 'Eiswürfel', 'Schokoeis', 'Erdbeereis', 'Milch', 'Salamie', 'Toilettenpapier', "Tomaten"]
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            ),*/

              Container(
                margin: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text("Menge     "),
                    ),
                    Container(
                      child: Text("${Strings.paprika}"),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: RawMaterialButton(
                        onPressed: () => setState(() => Strings.paprika--),
                        elevation: 2.0,
                        fillColor: Colors.white,
                        child: Text(
                          "-",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        padding: EdgeInsets.all(15.0),
                        shape: CircleBorder(),
                      ),
                      width: 80.0,
                    ),
                    Container(
                      child: RawMaterialButton(
                        onPressed: () {
                          setState(() {
                            Strings.paprika--;
                            Strings.paprikatrash++;
                          });
                        },
                        elevation: 2.0,
                        fillColor: Colors.white,
                        child: Icon(Icons.restore_from_trash),
                        padding: EdgeInsets.all(15.0),
                        shape: CircleBorder(),
                      ),
                      width: 80.0,
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 5, top: 5, bottom: 5),
                alignment: Alignment.centerLeft,                child: Row(
                  children: <Widget>[
                    Container(
                      child: Text("Kategorie     "),
                    ),
                    Container(
                      child: DropdownButton<String>(
                        value: kategorieString,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 20,
                        onChanged: (String newValue) {
                          setState(() {
                            kategorieString = newValue;
                          });
                        },
                        items: <String>['Gemüse', 'Obst', 'Eis', 'Hygiene']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 5, top: 5, bottom: 5),
                alignment: Alignment.centerLeft,
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Text("Lagerort     "),
                    ),
                    Container(
                      child: DropdownButton<String>(
                        value: lagerortString,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 20,
                        onChanged: (String newValue) {
                          setState(() {
                            lagerortString = newValue;
                          });
                        },
                        items: <String>[
                          'Kühlschrank',
                          'Vorratsschrank',
                          'Tiefkühlfach'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    )
                  ],
                ),
              ),

              Container(
                margin: EdgeInsets.only(left: 20, right: 5, top: 5, bottom: 5),
                alignment: Alignment.centerLeft,                child: Row(
                  children: <Widget>[
                    Container(
                      child: Text("Mindestmenge     "),
                    ),
                    Container(
                      child: DropdownButton<String>(
                        value: mindestmengeString,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 20,
                        onChanged: (String newValue) {
                          setState(() {
                            mindestmengeString = newValue;
                          });
                        },
                        items: <String>[
                          '1',
                          '2',
                          '3'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    )
                  ],
                ),
              ),

              Container(
                margin: EdgeInsets.only(left: 20, right: 5, top: 5, bottom: 5),
                alignment: Alignment.centerLeft,                child: Row(
                  children: <Widget>[
                    Container(
                      child: Text("Einheit     "),
                    ),
                    Container(
                      child: DropdownButton<String>(
                        value: einheitString,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 20,
                        onChanged: (String newValue) {
                          setState(() {
                            einheitString = newValue;
                          });
                        },
                        items: <String>[
                          '15 gr.',
                          '250 ml',
                          '1 Stk.'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    )
                  ],
                ),
              ),

              Container(
                margin: EdgeInsets.only(left: 20, right: 5, top: 5, bottom: 5),
                alignment: Alignment.centerLeft,                child: Row(
                  children: <Widget>[
                    Container(
                      child: Text("Haltbarkeit     "),
                    ),
                    Container(
                      child: DropdownButton<String>(
                        value: mhdString,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 20,
                        onChanged: (String newValue) {
                          setState(() {
                            mhdString = newValue;
                          });
                        },
                        items: <String>[
                          '1 Tag',
                          '2 Tage',
                          '3 Tage',
                          '4 Tage',
                          '5 Tage',
                          '6 Tage',
                          '7 Tage',

                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    )
                  ],
                ),
              ),

              /*Container(child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text("Preis     "),
                ),
                Container(
                  child: DropdownButton<String>(
                    value: preisString,
                    icon: Icon(Icons.arrow_drop_down),
                    iconSize: 20,
                    onChanged: (String newValue) {
                      setState(() {
                        preisString = newValue;
                      });
                    },
                    items: <String>['1€', '2€', '3€', '4€']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),), */

              Container(
                margin: EdgeInsets.all(20.0),
                child: RaisedButton(
                  onPressed: () {
                    {
                      setState(() {});
                    }
                    Navigator.pop(context);
                  },
                  child: Text("Speichern"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
