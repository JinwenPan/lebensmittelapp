import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'addStockItems.dart';

class GroceryList extends StatefulWidget {
  @override
  _GroceryListState createState() => _GroceryListState();
}

class _GroceryListState extends State<GroceryList> {
  //Funktion Items der Liste hinzufügen
  void addItem(String item, int mengeinput) {
    bool flagOnList = false;
    setState(() {
      //Strings.itemsneed2.add([item, mengeinput, false]);

      if(Strings.itemsneed2.isEmpty){
        Strings.itemsneed2.add([item, mengeinput, false]); //Eintragen des Inputs in die Map der Einkaufsliste
      }
      //Falls if-Abfrage unzutreffend in diese Funktion springen
      else{
        flagOnList = false;
        //For-Schleife, um die Einträge der Liste durchzulaufen
        for (var i = 0; i < Strings.itemsneed2.length; i++) {
          //Ist die Eingabe aus dem Textfeld bereits in der Liste vorhanden, wird nur die Menge angepasst
          if (Strings.itemsneed2[i][0] == item) {
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1] + mengeinput;
            flagOnList = true;
            break;
          }
        }
          //Sicherstellen, dass ein Eintrag für die Texteingabe erstellt wird
          if (flagOnList == false) {
            Strings.itemsneed2.add([item, mengeinput, false]); //Eintragen des Inputs in die Map der Einkaufsliste
          }
      }

    });
    Navigator.of(context).pop();
  }

  //Funktion Items aus Liste löschen
  void deleteItem(String key) {
    setState(() {
      for (var i = 0; i < Strings.itemsneed2.length; i++) {
        if (Strings.itemsneed2[i][0] == key) {
          Strings.itemsneed2.removeAt(i);
          break;
        }
      }
    });
  }

  //Funktion zum Ändern des Zustandes der Checkboxen
  void toggleDone(String key) {
    setState(() {
      for (var i = 0; i < Strings.itemsneed2.length; i++) {
        if (Strings.itemsneed2[i][0] == key &&
            Strings.itemsneed2[i][2] == false) {
          Strings.itemsneed2[i][2] = true;
          break;
        } else if (Strings.itemsneed2[i][0] == key &&
            Strings.itemsneed2[i][2] == true) {
          Strings.itemsneed2[i][2] = false;
          break;
        }
      }
    });
  }

  //Hinzufügen neuer Listeneinträge bei geöffneter Einkaufsliste
  void newEntry() {
    showDialog<AlertDialog>(
      context: context,
      builder: (BuildContext context) {
        return AddItemDialog(addItem);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildGroceryListScreen();
  }

  Scaffold _buildGroceryListScreen() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(43, 125, 50, 1),
        title: Text("Deine Einkaufsliste"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              //Button Umleitung zu Einkauf scannen
              Container(
                margin: EdgeInsets.only(top: 2.0),
                height: MediaQuery.of(context).size.height / 30,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AddStockItems()),
                    ).then((value) => setState(() => {}));
                  },
                  child: const Text('Einkauf lagern',
                      style: TextStyle(fontSize: 20)),
                ),
              ),

              Container(
                height: MediaQuery.of(context).size.height / 1.5,
                child:
                    //Generierung der Einkaufsliste
                    ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: Strings.itemsneed2.length,
                        itemBuilder: (context, i) {
                          String key = Strings.itemsneed2[i][0];
                          int menge = Strings.itemsneed2[i][1];
                          bool check = Strings.itemsneed2[i][2];
                          return ItemGroceryList(
                            key,
                            menge,
                            check,
                            () => deleteItem(key),
                            () => toggleDone(key),
                          );
                        }),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromRGBO(43, 125, 50, 1),
        onPressed: newEntry,
        child: Icon(Icons.add),
      ),
    );
  }
}

class ItemGroceryList extends StatelessWidget {
  final String title;
  final int amount;
  final bool done;
  final Function remove;
  final Function toggleDone;
  const ItemGroceryList(
      this.title, this.amount, this.done, this.remove, this.toggleDone);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 22),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(vertical: 8.0),
        leading: Checkbox(
          value: done,
          activeColor: Color.fromRGBO(43, 125, 50, 1),
          onChanged: (bool value) => toggleDone(),
        ),
        title: Text(
          title,
          style: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            decoration: done ? TextDecoration.lineThrough : TextDecoration.none,
          ),
        ),
        subtitle: Text(
          'Menge: $amount',
          style: TextStyle(
            fontSize: 18.0,
          ),
        ),
        trailing: IconButton(
          icon: Icon(Icons.delete_outline),
          onPressed: () => remove(),
        ),
      ),
    );
  }
}

//Dialog für das Hinzufügen neuer Listeneinträge
class AddItemDialog extends StatefulWidget {
  final void Function(String txt, int inp) addItem;
  const AddItemDialog(this.addItem);

  @override
  _AddItemDialogState createState() => _AddItemDialogState();
}

class _AddItemDialogState extends State<AddItemDialog> {
  final GlobalKey<FormState> formKey = GlobalKey();
  String item;
  int mengeinput = 1;

  void save() {
    if (formKey.currentState.validate()) {
      widget.addItem(item, mengeinput);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Form(
        key: formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(), hintText: 'Produkt suchen'),
              onChanged: (String txt) => item = txt,
              onFieldSubmitted: (String txt) => save(),
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Bitte Produktname einfügen';
                }
                return null;
              },
            ),
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(10.0),
                    child: Center(
                      child: Text('Menge',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10.0),
                    child: Center(
                      child: DropdownButton<int>(
                        value: Strings.mengeneedinput,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 20,
                        onChanged: (int newValue) {
                          setState(() {
                            Strings.mengeneedinput = newValue;
                            mengeinput = Strings.mengeneedinput;
                          });
                        },
                        items: <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                            .map<DropdownMenuItem<int>>((int value) {
                          return DropdownMenuItem<int>(
                            value: value,
                            child: Text("$value"),
                          );
                        }).toList(),
                      ),
                    ),
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                ],
              ),
            ),
            RaisedButton(
              onPressed: save,
              color: Color.fromRGBO(43, 125, 50, 1),
              child: Text(
                'Hinzufügen',
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
