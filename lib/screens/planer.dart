import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'package:flutter_calendar_week/flutter_calendar_week.dart';
import 'package:prepareNcare/screens/recipes.dart';
import 'dart:core';

class Planer extends StatefulWidget {
  static const String routeName = Strings.routeSeite3;

  @override
  _PlanerState createState() => _PlanerState();
}

class _PlanerState extends State<Planer> {
  final CalendarWeekController _controller = CalendarWeekController();

  void addIngredientsToList(String zutat1, String zutat2, String zutat3) {

    int menge1 = int.parse(Strings.mengeZutat1);
    int menge2 = int.parse(Strings.mengeZutat2);
    int menge3 = int.parse(Strings.mengeZutat3);

    bool zutat1AufListe = false;
    bool zutat2AufListe = false;
    bool zutat3AufListe = false;

    //Check wether enough in Stock or add to List
    switch(zutat1){
      case "Paprika":
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat1){
            zutat1AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge1;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat1AufListe == false){
          if (menge1 > Strings.paprika){
            Strings.itemsneed2.add([zutat1, menge1 - Strings.paprika, false]);
          }
        }
        break;
      case "Käse":
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat1){
            zutat1AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge1;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat1AufListe == false){
          if (menge1 > Strings.cheese){
            Strings.itemsneed2.add([zutat1, menge1 - Strings.cheese, false]);
          }
        }
        break;
      case "Tomaten" :
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat1){
            zutat1AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge1;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat1AufListe == false){
          if (menge1 > Strings.tomatoes){
            Strings.itemsneed2.add([zutat1, menge1 - Strings.tomatoes, false]);
          }
        }
        break;
      default:
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat1){
            zutat1AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge1;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat1AufListe == false){
          Strings.itemsneed2.add([zutat1, menge1, false]);
        }
        break;
    }

    switch(zutat2){
      case "Paprika":
        //prüfen, ob Eintrag bereits vorhanden
        //nein -> neu anlegen
        //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat2){
            zutat2AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge2;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat2AufListe == false){
          if (menge2 > Strings.paprika){
            Strings.itemsneed2.add([zutat2, menge2 - Strings.paprika, false]);
          }
        }
        break;
      case "Käse":
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat2){
            zutat2AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge2;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat2AufListe == false){
          if (menge2 > Strings.cheese){
            Strings.itemsneed2.add([zutat2, menge2 - Strings.cheese, false]);
          }
        }
        break;
      case "Tomaten" :
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat2){
            zutat2AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge2;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat2AufListe == false){
          if (menge2 > Strings.tomatoes){
            Strings.itemsneed2.add([zutat2, menge2 - Strings.tomatoes, false]);
          }
        }
        break;
      default:
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat2){
            zutat2AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge2;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat2AufListe == false){
          Strings.itemsneed2.add([zutat2, menge2, false]);
        }
        break;
    }

    switch(zutat3){
      case "Paprika":
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat3){
            zutat3AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge3;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat3AufListe == false){
          if (menge3 > Strings.paprika){
            Strings.itemsneed2.add([zutat3, menge3 - Strings.paprika, false]);
          }
        }
        break;
      case "Käse":
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat3){
            zutat3AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge3;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat3AufListe == false){
          if (menge3 > Strings.cheese){
            Strings.itemsneed2.add([zutat3, menge3 - Strings.cheese, false]);
          }
        }
        break;
      case "Tomaten" :
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat3){
            zutat3AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge3;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat3AufListe == false){
          if (menge3 > Strings.tomatoes){
            Strings.itemsneed2.add([zutat3, menge3 - Strings.tomatoes, false]);
          }
        }
        break;
      default:
        //prüfen, ob Eintrag bereits vorhanden
      //nein -> neu anlegen
      //ja -> neue Menge = vorhandene Menge + menge1
        for (var i = 0; i < Strings.itemsneed2.length; i++){
          if (Strings.itemsneed2[i][0] == zutat3){
            zutat3AufListe = true;
            Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1]+menge3;
            Strings.itemsneed2[i][2] = false;
            break;
          }
        }
        if (zutat3AufListe == false){
          Strings.itemsneed2.add([zutat3, menge3, false]);
        }
        break;
    }

    setState(() {});
  }

  datePressed(DateTime datetime){
    bool gefunden = false;

    // Zutaten + Beschreibung durch gemockte Strings (ZutatHotDog1, usw.; BeschreibungHotDog, ...)
    // Menge als Zahl -> Anzeigen * Personenzahl

    for(var i = 0; i < Strings.row; i++){
      if (Strings.geplanteRezepte[i][1] == datetime.day){
        //abgleich String -> passendes Bild in vorschauBild
        if (Strings.geplanteRezepte[i][0] == Strings.gnocchi){
          Strings.vorschauBild = Strings.gnocchiBild;
          Strings.titelRezept = 'Gnocchi';
          Strings.zutat1 = Strings.zutatGnocchi1;
          Strings.zutat2 = Strings.zutatGnocchi2;
          Strings.zutat3 = Strings.zutatGnocchi3;
          Strings.mengeZutat1 = (Strings.mengeZutatGnocchi1*Strings.anzahlPersonen).toString();
          Strings.mengeZutat2 = (Strings.mengeZutatGnocchi2*Strings.anzahlPersonen).toString();
          Strings.mengeZutat3 = (Strings.mengeZutatGnocchi3*Strings.anzahlPersonen).toString();
          Strings.beschreibung = Strings.beschreibungGnocchi;
          Strings.addGroceryVisible = true;
          gefunden = true;
        }
        else if (Strings.geplanteRezepte[i][0] == Strings.fleischGemuesePfanne){
          Strings.vorschauBild = Strings.fleischGemuesePfanneBild;
          Strings.titelRezept = 'Fleisch-Gemüse-Pfanne';
          Strings.zutat1 = Strings.zutatFleischGemuesePfanne1;
          Strings.zutat2 = Strings.zutatFleischGemuesePfanne2;
          Strings.zutat3 = Strings.zutatFleischGemuesePfanne3;
          Strings.mengeZutat1 = (Strings.mengeZutatFleischGemuesePfanne1*Strings.anzahlPersonen).toString();
          Strings.mengeZutat2 = (Strings.mengeZutatFleischGemuesePfanne2*Strings.anzahlPersonen).toString();
          Strings.mengeZutat3 = (Strings.mengeZutatFleischGemuesePfanne3*Strings.anzahlPersonen).toString();
          Strings.beschreibung = Strings.beschreibungFleischGemuesePfanne;
          Strings.addGroceryVisible = true;
          gefunden = true;
        }
        else if (Strings.geplanteRezepte[i][0] == Strings.hotdogs){
          Strings.vorschauBild = Strings.hotdogsBild;
          Strings.titelRezept = 'Hot Dogs';
          Strings.zutat1 = Strings.zutatHotDog1;
          Strings.zutat2 = Strings.zutatHotDog2;
          Strings.zutat3 = Strings.zutatHotDog3;
          Strings.mengeZutat1 = (Strings.mengeZutatHotDog1*Strings.anzahlPersonen).toString();
          Strings.mengeZutat2 = (Strings.mengeZutatHotDog2*Strings.anzahlPersonen).toString();
          Strings.mengeZutat3 = (Strings.mengeZutatHotDog3*Strings.anzahlPersonen).toString();
          Strings.beschreibung = Strings.beschreibungHotDog;
          Strings.addGroceryVisible = true;
          gefunden = true;
        }
        else if (Strings.geplanteRezepte[i][0] == Strings.nudelauflauf){
          Strings.vorschauBild = Strings.nudelauflaufBild;
          Strings.titelRezept = 'Nudelauflauf';
          Strings.zutat1 = Strings.zutatNudelauflauf1;
          Strings.zutat2 = Strings.zutatNudelauflauf2;
          Strings.zutat3 = Strings.zutatNudelauflauf3;
          Strings.mengeZutat1 = (Strings.mengeZutatNudelauflauf1*Strings.anzahlPersonen).toString();
          Strings.mengeZutat2 = (Strings.mengeZutatNudelauflauf2*Strings.anzahlPersonen).toString();
          Strings.mengeZutat3 = (Strings.mengeZutatNudelauflauf3*Strings.anzahlPersonen).toString();
          Strings.beschreibung = Strings.beschreibungNudelauflauf;
          Strings.addGroceryVisible = true;
          gefunden = true;
        }
        break;
      }
    }

    if (gefunden!=true){
      Strings.vorschauBild = 'lib/assets/empty.png';
      Strings.titelRezept = '';
      Strings.zutat1 = '';
      Strings.zutat2 = '';
      Strings.zutat3 = '';
      Strings.mengeZutat1 = '';
      Strings.mengeZutat2 = '';
      Strings.mengeZutat3 = '';
      Strings.beschreibung = '';
      Strings.addGroceryVisible = false;
    }

    setState(() {});
  }

  showConfirmDialog(BuildContext context) {  // set up the button
    Widget okButton = FlatButton(
      child: Text("OK", style: TextStyle(color: Color.fromRGBO(43, 125, 50, 1)),),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );  // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Erfolgreich hinzugefügt"),
      content: Text("Die Zutaten für dieses Rezept wurden auf deine Einkaufsliste gesetzt."),
      actions: [
        okButton,
      ],
    );  // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildPlanerScreen();
  }

  //Get Date of this weeks Monday
  var lastMonday = DateTime.now().subtract(Duration(days: DateTime.now().weekday - 1));


  Scaffold _buildPlanerScreen() {


    //List<DecorationItems>
    DecorationItem itemRezept1 = new DecorationItem(
        date: lastMonday.add(Duration(days: 1)),
        decoration: Icon(
          Icons.check,
          color: Color.fromRGBO(43, 125, 50, 1),
          semanticLabel: Strings.gnocchi,
        ));
    DecorationItem itemRezept2 = new DecorationItem(
        date: lastMonday.add(Duration(days: 2)),
        decoration: Icon(
          Icons.check,
          color: Color.fromRGBO(43, 125, 50, 1),
          semanticLabel: Strings.fleischGemuesePfanne,
        ));
    DecorationItem itemRezept3 = new DecorationItem(
        date: lastMonday.add(Duration(days: 4)),
        decoration: Icon(
          Icons.check,
          color: Color.fromRGBO(43, 125, 50, 1),
          semanticLabel: Strings.hotdogs,
        ));

    // checken, ob Beispiele bereits in Liste sind
    bool flagDatumBelegt = false;
    for(var i = 0; i < Strings.decorationItems.length; i++){
      if (Strings.decorationItems[i].date.day == itemRezept1.date.day){
        flagDatumBelegt = true;
      }
    }
    if (flagDatumBelegt == false){
      Strings.decorationItems.add(itemRezept1);
      Strings.geplanteRezepte[itemRezept1.date.day-1][0] = Strings.gnocchi;
      Strings.geplanteRezepte[itemRezept1.date.day-1][1] = itemRezept1.date.day;
    }
    flagDatumBelegt = false;

    for(var i = 0; i < Strings.decorationItems.length; i++){
      if (Strings.decorationItems[i].date.day == itemRezept2.date.day){
        flagDatumBelegt = true;
      }
    }
    if (flagDatumBelegt == false){
      Strings.decorationItems.add(itemRezept2);
      Strings.geplanteRezepte[itemRezept2.date.day-1][0] = Strings.fleischGemuesePfanne;
      Strings.geplanteRezepte[itemRezept2.date.day-1][1] = itemRezept2.date.day;
    }
    flagDatumBelegt = false;

    for(var i = 0; i < Strings.decorationItems.length; i++){
      if (Strings.decorationItems[i].date.day == itemRezept3.date.day){
        flagDatumBelegt = true;
      }
    }
    if (flagDatumBelegt == false){
      Strings.decorationItems.add(itemRezept3);
      Strings.geplanteRezepte[itemRezept3.date.day-1][0] = Strings.hotdogs;
      Strings.geplanteRezepte[itemRezept3.date.day-1][1] = itemRezept3.date.day;
    }
    flagDatumBelegt = false;


    void updateDecorationItems(String titel, DateTime date){
      // wenn titel == '' -> exit;
      if (titel == ''){
        return;
      }
      // wenn date bereits belegt -> Eintrag aus decorationItems entfernen
      for(var i = 0; i < Strings.decorationItems.length; i++){
        if (Strings.decorationItems[i].date.day == date.day){
          Strings.decorationItems.removeAt(i);
        }
      }

        DecorationItem itemAdd = new DecorationItem(
            date: date,
            decoration: Icon(
              Icons.check,
              color: Color.fromRGBO(43, 125, 50, 1),
              semanticLabel: titel,
            ));
        Strings.decorationItems.add(itemAdd);

        Strings.geplanteRezepte[date.day-1][0] = titel;
        Strings.geplanteRezepte[date.day-1][1] = date.day;


      setState(() {});

      Strings.rezeptGewaehlt = '';
      Strings.rezeptDatum = null;
    }

    return Scaffold(
      body: Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            //Calendar view
            Container(
              child: CalendarWeek(
                controller: _controller,
                height: 100,
                showMonth: true,
                monthStyle: TextStyle(
                  color: Color.fromRGBO(233, 83, 31, 1),
                ),
                weekendsStyle: TextStyle(
                  color: Color.fromRGBO(233, 83, 31, 1),
                ),
                dateStyle: TextStyle(
                  color: Color.fromRGBO(233, 83, 31, 1),
                ),
                dayOfWeekStyle: TextStyle(
                  color: Color.fromRGBO(43, 125, 50, 1),
                ),
                todayDateStyle: TextStyle(
                  color: Color.fromRGBO(43, 125, 50, 1),
                ),
                minDate: lastMonday,
                maxDate: lastMonday.add(
                  Duration(days: 27),
                ),
                //Passendes Rezept anzeigen, wenn ein Tag ausgewählt wird
                onDatePressed: (DateTime datetime) {
                  datePressed(datetime);
                },
                onDateLongPressed: (DateTime datetime) {
                  // Rezept auswählen

                  // prüfen, ob Beispiel gewählt
                  bool flagBeispiel = false;
                  if (datetime.day == lastMonday.day+1 || datetime.day == lastMonday.day+2 || datetime.day == lastMonday.day+4){
                    Strings.titelRezept = 'Der Beispieleintrag kann nicht verändert werden';
                    setState(() {});
                    flagBeispiel = true;
                  }

                  if (flagBeispiel == false) {
                    //gewähltes Datum sichern
                    Strings.rezeptDatum = datetime;
                    Strings.rezeptGewaehlt = '';

                    //Reset values before push
                    //Status Zutaten für Rezepte filtern
                    Strings.kaesePlus = false;
                    Strings.kaeseMinus = false;
                    Strings.tomatenPlus = false;
                    Strings.tomatenMinus = false;
                    Strings.paprikaPlus = false;
                    Strings.paprikaMinus = false;

                    //Farben für Filterbuttons regeln
                    Strings.kaesePlusColor = Colors.white;
                    Strings.kaeseMinusColor = Colors.white;
                    Strings.tomatenPlusColor = Colors.white;
                    Strings.tomatenMinusColor = Colors.white;
                    Strings.paprikaPlusColor = Colors.white;
                    Strings.paprikaMinusColor = Colors.white;

                    //Rezepte filtern
                    Strings.gnocchiVisible = true;
                    Strings.nudelauflaufVisible = true;
                    Strings.fleischgemuesepfanneVisible = true;
                    Strings.hotdogsVisible = true;

                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Recipes()),
                    ).then((context) {
                      updateDecorationItems(Strings.rezeptGewaehlt, Strings.rezeptDatum);
                      datePressed(datetime);
                      }
                    );
                  }
                },
                decorations: Strings.decorationItems,
              ),
            ),
            //Personenanzahl auswählen
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              height: MediaQuery.of(context).size.height / 2,
              child: ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                Strings.addGroceryVisible ? Container(
                child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text("Anzahl der Personen     "),
                  ),
                  Container(
                    child: DropdownButton<int>(
                      value: Strings.anzahlPersonen,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 16,
                      onChanged: (int newValue) {
                        Strings.personenOld = Strings.anzahlPersonen;
                        Strings.anzahlPersonen = newValue;

                        //neue Mengen berechen
                        Strings.mengeZutat1 = ((int.parse(Strings.mengeZutat1)/Strings.personenOld)*Strings.anzahlPersonen).toInt().toString();
                        Strings.mengeZutat2 = ((int.parse(Strings.mengeZutat2)/Strings.personenOld)*Strings.anzahlPersonen).toInt().toString();
                        Strings.mengeZutat3 = ((int.parse(Strings.mengeZutat3)/Strings.personenOld)*Strings.anzahlPersonen).toInt().toString();
                        setState(() {});
                      },
                      items: <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                          .map<DropdownMenuItem<int>>((int value) {
                        return DropdownMenuItem<int>(
                          value: value,
                          child: Text("$value"),
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            ) : new Container(),
            Container(
              child: Text(
                Strings.titelRezept,
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: Image.asset(Strings.vorschauBild),
                    width: 150.0,
                    alignment: Alignment.center,
                    height: MediaQuery.of(context).size.height / 6,
                  ),
                  Container(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      Strings.zutat1,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      Strings.zutat2,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      Strings.zutat3,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      '   ',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      '   ',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      '   ',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      Strings.mengeZutat1,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      Strings.mengeZutat2,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      Strings.mengeZutat3,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ]
                      )
                  ),
                ],
              ),
            ),
              Strings.addGroceryVisible ? Container(
                height: MediaQuery.of(context).size.height / 24,
                margin: EdgeInsets.symmetric(vertical: 10.0),
                alignment: Alignment.bottomCenter,
                child: RaisedButton(
                  onPressed: () {
                    addIngredientsToList(Strings.zutat1, Strings.zutat2, Strings.zutat3);
                    showConfirmDialog(context);
                  },
                color: Colors.white,
                child: Text(
                  "Zutaten auf Einkaufsliste setzen",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                //padding: EdgeInsets.all(15.0),
              ),
            ) : new Container(),

            Container(
              child: Text(
                '',
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 16),
              ),
            ),
                  Strings.addGroceryVisible ? Container(
                    child: Text(
                      'Rezeptbeschreibung',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ) : new Container(),
                  //Hinweis Rezept hinzufügen, wenn keins angezeigt wird
                  !Strings.addGroceryVisible ? Container(
                    child: Text(
                      'Zum Hinzufügen eines Rezeptes lange auf den entsprechenden Tag drücken',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ) : new Container(),
                  Container(
                    child: Text(
                      '',
                      textAlign: TextAlign.start,
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
            Container(
              child: Text(
                Strings.beschreibung,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16),
              ),
            ),
          ],
        ),
      ),
      ],
          ),
      ),
    );
  }
}
