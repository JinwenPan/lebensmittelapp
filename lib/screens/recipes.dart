import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';

class Recipes extends StatefulWidget {

  @override
  _RecipesState createState() => _RecipesState();
}


class _RecipesState extends State<Recipes> {
  @override
  Widget build(BuildContext context) {

    //bool Werte für Rezeptanzeige setzen
    void updateRecipesVisibility(){

      //Gnocchi prüfen
      //wenn Käse oder Tomaten -, dann nicht
      //wenn Paprika +, dann nicht
      if (Strings.kaeseMinus == true || Strings.tomatenMinus == true || Strings.paprikaPlus == true){
        Strings.gnocchiVisible = false;
      }
      else{
        Strings.gnocchiVisible = true;
      }

      //Nudelauflauf prüfen
      //s. Gnocchi
      if (Strings.kaeseMinus == true || Strings.tomatenMinus == true || Strings.paprikaPlus == true){
        Strings.nudelauflaufVisible = false;
      }
      else{
        Strings.nudelauflaufVisible = true;
      }

      //Fleisch-Gemüse-Pfanne prüfen
      //wenn Paprika -, dann nicht
      //wenn Käse oder Tomaten +, dann nicht
      if (Strings.kaesePlus == true || Strings.tomatenPlus == true || Strings.paprikaMinus == true){
        Strings.fleischgemuesepfanneVisible = false;
      }
      else{
        Strings.fleischgemuesepfanneVisible = true;
      }

      //Hot Dogs prüfen
      //wenn Käse, Tomaten oder Paprika +, dann nicht
      if (Strings.kaesePlus == true || Strings.tomatenPlus == true || Strings.paprikaPlus == true){
        Strings.hotdogsVisible = false;
      }
      else{
        Strings.hotdogsVisible = true;
      }
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(43, 125, 50, 1),
        title: Text("Rezept auswählen"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text(
                  '   ',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16),
                ),
              ),
              //Überschrift Zutaten inkludieren
              Container(
                child: Text(
                  "Zutaten hinzufügen oder ausschließen",
                  textAlign: TextAlign.start,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),

              //Zutaten Elemente
              Container(
                margin: EdgeInsets.symmetric(vertical: 20.0),
                height: MediaQuery.of(context).size.height / 6,
                child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      //Hier beginnt der Container für ein Element

                      Container(
                        child: ListView(
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Käse:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: MediaQuery.of(context).size.height / 24,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: MediaQuery.of(context).size.height / 24,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.cheese}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: MediaQuery.of(context).size.height / 24,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/cheese.png'),
                              width: 80.0,
                              height: MediaQuery.of(context).size.height / 24,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(() {
                                        if (Strings.kaeseMinus == false) {
                                          Strings.kaeseMinus = true;
                                          Strings.kaesePlus = false;
                                          Strings.kaeseMinusColor = Colors.red;
                                          Strings.kaesePlusColor = Colors.white;
                                        }
                                        else{
                                          Strings.kaeseMinus = false;
                                          Strings.kaeseMinusColor = Colors.white;
                                        }
                                        updateRecipesVisibility();
                                      });
                                    },
                                    elevation: 2.0,
                                    fillColor: Strings.kaeseMinusColor,
                                    child: Text(
                                      "-",
                                      style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(() {
                                        if (Strings.kaesePlus == false) {
                                          Strings.kaesePlus = true;
                                          Strings.kaeseMinus = false;
                                          Strings.kaesePlusColor = Colors.green;
                                          Strings.kaeseMinusColor = Colors.white;
                                        }
                                        else{
                                          Strings.kaesePlus = false;
                                          Strings.kaesePlusColor = Colors.white;
                                        }
                                        updateRecipesVisibility();
                                      });
                                    },
                                    elevation: 2.0,
                                    fillColor: Strings.kaesePlusColor,
                                    child: Text(
                                      "+",
                                      style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),

                      //Hier beginnt der Container für ein Element

                      Container(
                        child: ListView(
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Paprika:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: MediaQuery.of(context).size.height / 24,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: MediaQuery.of(context).size.height / 24,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.paprika}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: MediaQuery.of(context).size.height / 24,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/bell-peppers.png'),
                              width: 80.0,
                              height: MediaQuery.of(context).size.height / 24,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(() {
                                        if (Strings.paprikaMinus == false) {
                                          Strings.paprikaMinus = true;
                                          Strings.paprikaPlus = false;
                                          Strings.paprikaMinusColor = Colors.red;
                                          Strings.paprikaPlusColor = Colors.white;
                                        }
                                        else{
                                          Strings.paprikaMinus = false;
                                          Strings.paprikaMinusColor = Colors.white;
                                        }
                                        updateRecipesVisibility();
                                      });
                                    },
                                    elevation: 2.0,
                                    fillColor: Strings.paprikaMinusColor,
                                    child: Text(
                                      "-",
                                      style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(() {
                                        if (Strings.paprikaPlus == false) {
                                          Strings.paprikaPlus = true;
                                          Strings.paprikaMinus = false;
                                          Strings.paprikaPlusColor = Colors.green;
                                          Strings.paprikaMinusColor = Colors.white;
                                        }
                                        else{
                                          Strings.paprikaPlus = false;
                                          Strings.paprikaPlusColor = Colors.white;
                                        }
                                        updateRecipesVisibility();
                                      });
                                    },
                                    elevation: 2.0,
                                    fillColor: Strings.paprikaPlusColor,
                                    child: Text(
                                      "+",
                                      style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      ),

                      //Hier beginnt der Container für ein Element

                      Container(
                        child: ListView(
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Tomaten:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: MediaQuery.of(context).size.height / 24,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: MediaQuery.of(context).size.height / 24,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.tomatoes}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: MediaQuery.of(context).size.height / 24,
                                ),
                              ],
                            ),
                            Container(
                              child: Image.asset('lib/assets/tomatoes.png'),
                              width: 80.0,
                              height: MediaQuery.of(context).size.height / 24,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(() {
                                        if (Strings.tomatenMinus == false) {
                                          Strings.tomatenMinus = true;
                                          Strings.tomatenPlus = false;
                                          Strings.tomatenMinusColor = Colors.red;
                                          Strings.tomatenPlusColor = Colors.white;
                                        }
                                        else{
                                          Strings.tomatenMinus = false;
                                          Strings.tomatenMinusColor = Colors.white;
                                        }
                                        updateRecipesVisibility();
                                      });
                                    },
                                    elevation: 2.0,
                                    fillColor: Strings.tomatenMinusColor,
                                    child: Text(
                                      "-",
                                      style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      setState(() {
                                        if (Strings.tomatenPlus == false) {
                                          Strings.tomatenPlus = true;
                                          Strings.tomatenMinus = false;
                                          Strings.tomatenPlusColor = Colors.green;
                                          Strings.tomatenMinusColor = Colors.white;
                                        }
                                        else{
                                          Strings.tomatenPlus = false;
                                          Strings.tomatenPlusColor = Colors.white;
                                        }
                                        updateRecipesVisibility();
                                      });
                                    },
                                    elevation: 2.0,
                                    fillColor: Strings.tomatenPlusColor,
                                    child: Text(
                                      "+",
                                      style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                        //color: Colors.red,
                      )
                    ]),
              ),

              //Überschrift Rezepte
              Container(
                child: Text(
                  "Passende Rezepte",
                  textAlign: TextAlign.start,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
              //Rezepte Elemente
              Container(
                margin: EdgeInsets.symmetric(vertical: 20.0),
                height: MediaQuery.of(context).size.height / 2,
                width: MediaQuery.of(context).size.width - (MediaQuery.of(context).size.width/9),
                child: ListView(
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                              Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    //Beginn eines Elements
                                    Strings.nudelauflaufVisible ? Container(
                                      height: MediaQuery.of(context).size.height / 6,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                child: GestureDetector(
                                                  onTap: () {
                                                    //Update rezeptGewaehlt + zurück zu Planer
                                                    Strings.rezeptGewaehlt = Strings.nudelauflauf;
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    Strings.nudelauflauf,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                ),
                                                height: MediaQuery.of(context).size.height / 24,
                                              ),
                                          Container(
                                            child: GestureDetector(
                                              onTap: () {
                                                //Update rezeptGewaehlt + zurück zu Planer
                                                Strings.rezeptGewaehlt = Strings.nudelauflauf;
                                                Navigator.pop(context);
                                              },
                                              child: Image.asset(Strings.nudelauflaufBild),
                                            ),
                                            height: MediaQuery.of(context).size.height / 10,
                                          ),
                                        ],
                                      ),
                                      //color: Colors.red,
                                    ) : new Container(),
                                    //Beginn eines Elements
                                    Strings.fleischgemuesepfanneVisible ? Container(
                                      height: MediaQuery.of(context).size.height / 6,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                child: GestureDetector(
                                                  onTap: () {
                                                    //Update rezeptGewaehlt + zurück zu Planer
                                                    Strings.rezeptGewaehlt = Strings.fleischGemuesePfanne;
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    Strings.fleischGemuesePfanne,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                ),
                                                height: MediaQuery.of(context).size.height / 24,
                                              ),
                                          Container(
                                            child: GestureDetector(
                                              onTap: () {
                                                //Update rezeptGewaehlt + zurück zu Planer
                                                Strings.rezeptGewaehlt = Strings.fleischGemuesePfanne;
                                                Navigator.pop(context);
                                              },
                                              child: Image.asset(Strings.fleischGemuesePfanneBild),),
                                            height: MediaQuery.of(context).size.height / 10,

                                          ),
                                        ],
                                      ),
                                      //color: Colors.red,
                                    ) : new Container(),
                                    //Beginn eines Elements
                                    Strings.gnocchiVisible ? Container(
                                      height: MediaQuery.of(context).size.height / 6,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                child: GestureDetector(
                                                  onTap: () {
                                                    //Update rezeptGewaehlt + zurück zu Planer
                                                    Strings.rezeptGewaehlt = Strings.gnocchi;
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    Strings.gnocchi,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                ),
                                                height: MediaQuery.of(context).size.height / 24,
                                              ),
                                          Container(
                                            child: GestureDetector(
                                              onTap: () {
                                                //Update rezeptGewaehlt + zurück zu Planer
                                                Strings.rezeptGewaehlt = Strings.gnocchi;
                                                Navigator.pop(context);
                                              },
                                              child: Image.asset(Strings.gnocchiBild),),
                                            height: MediaQuery.of(context).size.height / 10,

                                          ),
                                        ],
                                      ),
                                      //color: Colors.red,
                                    ) : new Container(),
                                    //Beginn eines Elements
                                    Strings.hotdogsVisible ? Container(
                                      height: MediaQuery.of(context).size.height / 6,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                child: GestureDetector(
                                                  onTap: () {
                                                    //Update rezeptGewaehlt + zurück zu Planer
                                                    Strings.rezeptGewaehlt = Strings.hotdogs;
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    Strings.hotdogs,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                ),
                                                height: MediaQuery.of(context).size.height / 24,
                                              ),
                                          Container(
                                            child: GestureDetector(
                                              onTap: () {
                                                //Update rezeptGewaehlt + zurück zu Planer
                                                Strings.rezeptGewaehlt = Strings.hotdogs;
                                                Navigator.pop(context);
                                              },
                                              child: Image.asset(Strings.hotdogsBild),),
                                            height: MediaQuery.of(context).size.height / 10,

                                          ),
                                        ],
                                      ),
                                      //color: Colors.red,
                                    ) : new Container(),
                                  ],
                                ),
                              ),
                            ],
                        ),
              ),
            ]),
      ),
    );
  }
}
