import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'dart:math';

class AddList extends StatefulWidget {
  //static const String routeName = Strings.routeSeite4;

  @override
  _AddListState createState() => _AddListState();
}

class _AddListState extends State<AddList> {

  int numberbellpepper = 3;
  String pricebellpepper = "2€";
  int numbertoiletpaper = 6;
  String pricetoiletpaper = "3€";

  @override
  Widget build(BuildContext context) {
    return _buildAddListScreen();
  }

  Scaffold _buildAddListScreen() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(43, 125, 50, 1),
        title: Text("Scan überprüfen"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            Container(
              alignment: Alignment.center,
              child: Text("Folgende Produkte wurden erkannt:"),
              height: MediaQuery.of(context).size.height / 5,

            ),

            Container(

              child: Row(
                children: <Widget>[
                  Container(
                    child: Center(
                      child: Text("Produkt"),
                    ),
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                  Container(
                    child: Center(
                      child: Text("Menge"),
                    ),
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                 /* Container(
                    child: Center(
                      child: Text("Kategorie"),
                    ),
                    width: MediaQuery.of(context).size.width / 5,
                  ),*/
                  Container(
                    child: Center(
                      child: Text("Preis"),
                    ),
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                 /* Container(
                    child: Center(
                      child: Text("Lagerort"),
                    ),
                    width: MediaQuery.of(context).size.width / 5,
                  ),*/
                ],
              ),
            ),

            Container(
              height: MediaQuery.of(context).size.height / 3,

              child: ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  Container(child: Row(
                children: <Widget>[
                  Container(
                    child: Center(
                      child: Text("Paprika"),
                    ),
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                  Container(
                    child: Center(child: DropdownButton<int>(
                      value: numberbellpepper,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 20,
                      onChanged: (int newValue) {
                        setState(() {
                          numberbellpepper = newValue;
                        });
                      },
                      items: <int>[1,2,3,4,5,6,7,8,10]
                          .map<DropdownMenuItem<int>>((int value) {
                        return DropdownMenuItem<int>(
                          value: value,
                          child: Text("$value"),
                        );
                      }).toList(),
                    ),),
                    width: MediaQuery.of(context).size.width / 3,
                  ),


                  /*Container(
                    child: Center(
                      child: Text("Gemüse"),
                    ),
                    width: MediaQuery.of(context).size.width / 5,
                  ),*/
                  Container(
                    child: Center(child: DropdownButton<String>(
                      value: pricebellpepper,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 20,
                      onChanged: (String newValue) {
                        setState(() {
                          pricebellpepper = newValue;
                        });
                      },
                      items: <String>["1€", "2€", "3€"]
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),),
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                  /*Container(
                    child: Center(
                      child: Text("Kühlschrank"),
                    ),
                    width: MediaQuery.of(context).size.width / 5,
                  ),*/
                ],
            ),),

            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    child: Center(
                      child: Text("Toilettenpapier"),
                    ),
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                  Container(
                    child: Center(child: DropdownButton<int>(
                      value: numbertoiletpaper,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 20,
                      onChanged: (int newValue) {
                        setState(() {
                          numbertoiletpaper = newValue;
                        });
                      },
                      items: <int>[1,2,3,4,5,6,7,8,10]
                          .map<DropdownMenuItem<int>>((int value) {
                        return DropdownMenuItem<int>(
                          value: value,
                          child: Text("$value"),
                        );
                      }).toList(),
                    ),),
                    width: MediaQuery.of(context).size.width / 3,
                  ),


                 /* Container(
                    child: Center(
                      child: Text("Hygiene"),
                    ),
                    width: MediaQuery.of(context).size.width / 5,
                  ),*/
                  Container(
                    child: Center(child: DropdownButton<String>(
                      value: pricetoiletpaper,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 20,
                      onChanged: (String newValue) {
                        setState(() {
                          pricetoiletpaper = newValue;
                        });
                      },
                      items: <String>["1€", "2€", "3€"]
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),),
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                  /*Container(
                    child: Center(
                      child: Text("Vorratsschrank"),
                    ),
                    width: MediaQuery.of(context).size.width / 5,
                  ),*/
                ],
              ),
            ),




            Container(
              alignment: Alignment.bottomCenter,
              child: RaisedButton(
                onPressed: () {
                  Strings.paprika = Strings.paprika + numberbellpepper;
                  Strings.toiletpaper = Strings.toiletpaper + numbertoiletpaper;
                 /* Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);*/
                  Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));

                },
                child: Text("Alles OK!"),
              ),
            ),
          ],
        ),
      ),
    ],),),
    );
  }
}
