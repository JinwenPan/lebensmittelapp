import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'dart:math';

import 'package:prepareNcare/screens/addList.dart';

class Scan extends StatefulWidget {
  //static const String routeName = Strings.routeSeite4;

  @override
  _ScanState createState() => _ScanState();
}

class _ScanState extends State<Scan> {



  @override
  Widget build(BuildContext context) {
    return _buildScanScreen();
  }

  Scaffold _buildScanScreen() {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(43, 125, 50, 1),
        title: Text("Scan"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => AddList()),);
          },
          child: const Text("Scan!"),
        )
      ),
    );
  }
}
