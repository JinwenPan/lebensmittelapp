import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'package:prepareNcare/screens/detailPage.dart';
import 'package:prepareNcare/screens/showGroceryList.dart';

class Grocery extends StatefulWidget {
  Grocery({Key key, this.title}) : super(key: key);
  static const String routeName = Strings.routeSeite2;
  String title;

  @override
  _GroceryState createState() => _GroceryState();
}

class _GroceryState extends State<Grocery> {

  bool flagOnList = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                //Button Einkaufsliste anzeigen
                Container(
                    height: MediaQuery.of(context).size.height / 30,
                    margin: EdgeInsets.symmetric(vertical: 25.0),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GroceryList()),
                        ).then((value) => setState(() {}));
                      },
                      child: const Text('Einkaufsliste anzeigen',
                          style: TextStyle(fontSize: 20)),
                    )),

                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        child: Center(
                          child: Text('Produkt',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        width: MediaQuery.of(context).size.width / 3,
                        margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 12,
                        ),
                      ),
                      Container(
                        child: Center(
                          child: Text('Menge',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        width: MediaQuery.of(context).size.width / 4,
                      ),
                      Container(
                        child: Center(
                          child: Text('Auf die Liste',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        width: MediaQuery.of(context).size.width / 4,
                        margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width / 12,
                        ),
                      ),
                    ],
                  ),
                ),

                //Produkte manuell eingeben
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        child: Center(
                          child: TextField(
                            controller: Strings.controller,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Produkt suchen'),
                          ),
                        ),
                        width: MediaQuery.of(context).size.width / 3,
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width / 12),
                      ),
                      Container(
                        child: Center(
                          child: DropdownButton<int>(
                            value: Strings.mengeneed,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 20,
                            onChanged: (int newValue) {
                              setState(() {
                                Strings.mengeneed = newValue;
                              });
                            },
                            items: <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                                .map<DropdownMenuItem<int>>((int value) {
                              return DropdownMenuItem<int>(
                                value: value,
                                child: Text("$value"),
                              );
                            }).toList(),
                          ),
                        ),
                        width: MediaQuery.of(context).size.width / 4,
                      ),
                      Container(
                        child: Center(
                          child: RaisedButton(
                            onPressed: () {

                              Strings.input = Strings.controller.text; //Auslesen des Textfeld Controllers

                              setState(() {

                                //Abfrage - Wenn Liste leer -> Neuer Eintrag
                                if(Strings.itemsneed2.isEmpty){
                                  Strings.itemsneed2.add([Strings.input, Strings.mengeneed, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                                  Strings.controller.clear(); //Textfeld Controller resetten
                                }

                                //Falls if-Abfrage unzutreffend in diese Funktion springen
                                else{
                                  flagOnList = false;

                                  //For-Schleife, um die Einträge der Liste durchzulaufen
                                  for (var i = 0; i < Strings.itemsneed2.length; i++) {
                                    //Ist die Eingabe aus dem Textfeld bereits in der Liste vorhanden, wird nur die Menge angepasst
                                    if (Strings.itemsneed2[i][0] == Strings.input) {
                                      Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1] + Strings.mengeneed;
                                      Strings.controller.clear(); //Textfeld Controller resetten
                                      flagOnList = true;
                                      break;
                                    }
                                  }

                                    //Sicherstellen, dass ein Eintrag für die Texteingabe erstellt wird
                                    if (flagOnList == false) {
                                      Strings.itemsneed2.add([Strings.input, Strings.mengeneed, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                                      Strings.controller.clear(); //Textfeld Controller resetten
                                    }
                                }
                              });
                            },
                            elevation: 2.0,
                            color: Colors.white,
                            child: Text(
                              "Liste",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            //padding: EdgeInsets.all(15.0),
                            shape: CircleBorder(),
                          ),
                        ),
                        width: MediaQuery.of(context).size.width / 4,
                        margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width / 12,
                        ),
                      ),
                    ],
                  ),
                ),

                //Überschrift geringer Vorrat
                Container(
                  margin: EdgeInsets.only(top: 25.0, bottom: 10.0),
                  child: Text("Geringer Vorrat",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18.0)),
                ),

                //Elemente geringer Vorrat
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20.0),
                  height: 160,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      //Container für das Element Milch
                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Milch:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.milk}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Detail()),
                                  ).then((value) => setState(() {}));
                                },
                                child: Image.asset('lib/assets/milk.png'),
                              ),
                              width: 80.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {

                                      String milch = 'Milch';
                                      Strings.milkneed ++;

                                      setState(() {
                                        if(Strings.itemsneed2.isEmpty){
                                          Strings.itemsneed2.add([milch, Strings.milkneed, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                                        }
                                        //Falls if-Abfrage unzutreffend in diese Funktion springen
                                        else{
                                          flagOnList = false;
                                          //For-Schleife, um die Einträge der Liste durchzulaufen
                                          for (var i = 0; i < Strings.itemsneed2.length; i++) {
                                            //Ist die Eingabe aus dem Textfeld bereits in der Liste vorhanden, wird nur die Menge angepasst
                                            if (Strings.itemsneed2[i][0] == milch) {
                                              Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1] + 1;
                                              flagOnList = true;
                                              break;
                                            }
                                          }

                                            //Sicherstellen, dass ein Eintrag für die Texteingabe erstellt wird
                                            if (flagOnList == false) {
                                              Strings.itemsneed2.add([milch, Strings.milkneed, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                                            }
                                        }
                                      });
                                    },

                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "Liste",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                      ),

                      //Container für das Element Tomaten
                      Container(
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Tomaten:",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "  ",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                                Container(
                                  child: Text(
                                    "${Strings.tomatoes}",
                                    textAlign: TextAlign.center,
                                  ),
                                  height: 50,
                                ),
                              ],
                            ),
                            Container(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Detail()),
                                  ).then((value) => setState(() {}));
                                },
                                child: Image.asset('lib/assets/tomatoes.png'),
                              ),
                              width: 80.0,
                              height: 50,
                            ),
                            new Row(
                              children: <Widget>[
                                Container(
                                  child: RawMaterialButton(
                                    onPressed: () {

                                      String tomaten = 'Tomaten';
                                      Strings.tomatoesneed ++;

                                      setState(() {
                                        if(Strings.itemsneed2.isEmpty){
                                          Strings.itemsneed2.add([tomaten, Strings.tomatoesneed, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                                        }
                                        //Falls if-Abfrage unzutreffend in diese Funktion springen
                                        else {
                                          flagOnList = false;
                                          //For-Schleife, um die Einträge der Liste durchzulaufen
                                          for (var i = 0; i < Strings.itemsneed2.length; i++) {
                                            //Ist die Eingabe aus dem Textfeld bereits in der Liste vorhanden, wird nur die Menge angepasst
                                            if (Strings.itemsneed2[i][0] == tomaten) {
                                              Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1] + 1;
                                              flagOnList = true;
                                              break;
                                            }
                                          }

                                            //Sicherstellen, dass ein Eintrag für die Texteingabe erstellt wird
                                          if (flagOnList == false) {
                                              Strings.itemsneed2.add([tomaten, Strings.tomatoesneed, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                                            }
                                        }
                                      });
                                        },
                                    elevation: 2.0,
                                    fillColor: Colors.white,
                                    child: Text(
                                      "Liste",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    padding: EdgeInsets.all(15.0),
                                    shape: CircleBorder(),
                                  ),
                                  width: 80.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                        width: 160.0,
                      ),
                    ],
                  ),
                ),

                //Überschrift Produktsuche
                Container(
                  margin: EdgeInsets.symmetric(vertical: 25.0),
                  child: Text(
                    "Produktsuche nach Kategorien",
                    textAlign: TextAlign.start,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                  ),
                ),

                Container(
                  child: Text("Obst",
                      textAlign: TextAlign.start,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ),

                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width / 12),
                        width: MediaQuery.of(context).size.width / 3,
                        child: DropdownButton<String>(
                          value: Strings.obstkategorie,
                          icon: Icon(Icons.arrow_drop_down),
                          iconSize: 20,
                          onChanged: (String newValue) {
                            setState(() {
                              Strings.obstkategorie = newValue;
                            });
                          },
                          items: <String>[
                            'Apfel',
                            'Erdbeeren',
                            'Bananen',
                            'Trauben'
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                      Container(
                        //margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width /16),
                        width: MediaQuery.of(context).size.width / 4,
                        child: Center(
                          child: DropdownButton<int>(
                            value: Strings.mengeneedobst,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 20,
                            onChanged: (int newValue) {
                              setState(() {
                                Strings.mengeneedobst = newValue;
                              });
                            },
                            items: <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                                .map<DropdownMenuItem<int>>((int value) {
                              return DropdownMenuItem<int>(
                                value: value,
                                child: Text("$value"),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            right: MediaQuery.of(context).size.width / 12,
                            top: 5,
                            bottom: 5),

                        width: MediaQuery.of(context).size.width / 4,
                        child: RawMaterialButton(
                          onPressed: () {
                            setState(() {

                              //Abfrage - Wenn Liste leer -> Neuer Eintrag
                              if(Strings.itemsneed2.isEmpty){
                                Strings.itemsneed2.add([Strings.obstkategorie, Strings.mengeneedobst, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                              }

                              //Falls if-Abfrage unzutreffend in diese Funktion springen
                              else{
                                flagOnList = false;

                                //For-Schleife, um die Einträge der Liste durchzulaufen
                                for (var i = 0; i < Strings.itemsneed2.length; i++) {
                                  //Ist die Eingabe aus dem Textfeld bereits in der Liste vorhanden, wird nur die Menge angepasst
                                  if (Strings.itemsneed2[i][0] == Strings.obstkategorie) {
                                    Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1] + Strings.mengeneedobst;
                                    flagOnList = true;
                                    break;
                                  }
                                }

                                  //Sicherstellen, dass ein Eintrag für die Texteingabe erstellt wird
                                  if (flagOnList == false) {
                                    Strings.itemsneed2.add([Strings.obstkategorie, Strings.mengeneedobst, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                                  }
                                }
                            });
                          },

                          /*
                          => setState(() => Strings.itemsneed2
                                  .add([
                                Strings.obstkategorie,
                                Strings.mengeneedobst,
                                false
                              ])),
                          */
                          elevation: 2.0,
                          fillColor: Colors.white,
                          child: Text(
                            "Liste",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          //padding: EdgeInsets.all(15.0),
                          shape: CircleBorder(),
                        ),
                        //width: 80.0,
                      ),
                    ],
                  ),
                ),

                Container(
                  child: Text("Gemüse",
                      textAlign: TextAlign.start,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ),

                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width / 12),
                        width: MediaQuery.of(context).size.width / 3,
                        //width: MediaQuery.of(context).size.width / 3,
                        child: DropdownButton<String>(
                          value: Strings.gemuesekategorie,
                          icon: Icon(Icons.arrow_drop_down),
                          iconSize: 20,
                          onChanged: (String newValue) {
                            setState(() {
                              Strings.gemuesekategorie = newValue;
                            });
                          },
                          items: <String>[
                            'Paprika',
                            'Tomaten',
                            'Brokkoli',
                            'Spinat',
                            'Kartoffeln'
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 4,
                        //width: MediaQuery.of(context).size.width / 3,
                        child: Center(
                          child: DropdownButton<int>(
                            value: Strings.mengeneedgemuese,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 20,
                            onChanged: (int newValue) {
                              setState(() {
                                Strings.mengeneedgemuese = newValue;
                              });
                            },
                            items: <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                                .map<DropdownMenuItem<int>>((int value) {
                              return DropdownMenuItem<int>(
                                value: value,
                                child: Text("$value"),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 4,
                        margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width / 12,
                        ),
                        //width: MediaQuery.of(context).size.width / 3,
                        child: RawMaterialButton(
                          onPressed: () {
                            setState(() {

                              //Abfrage - Wenn Liste leer -> Neuer Eintrag
                              if(Strings.itemsneed2.isEmpty){
                                Strings.itemsneed2.add([Strings.gemuesekategorie, Strings.mengeneedgemuese, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                              }

                              //Falls if-Abfrage unzutreffend in diese Funktion springen
                              else{
                                flagOnList = false;

                                //For-Schleife, um die Einträge der Liste durchzulaufen
                                for (var i = 0; i < Strings.itemsneed2.length; i++) {
                                  //Ist die Eingabe aus dem Textfeld bereits in der Liste vorhanden, wird nur die Menge angepasst
                                  if (Strings.itemsneed2[i][0] == Strings.gemuesekategorie) {
                                    Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1] + Strings.mengeneedgemuese;
                                    flagOnList = true;
                                    break;
                                  }
                                }

                                  //Sicherstellen, dass ein Eintrag für die Texteingabe erstellt wird
                                  if (flagOnList == false) {
                                    Strings.itemsneed2.add([Strings.gemuesekategorie, Strings.mengeneedgemuese, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                                  }
                              }
                            });
                          },
                          /*
                          => setState(() => Strings.itemsneed2
                                  .add([
                                Strings.gemuesekategorie,
                                Strings.mengeneedgemuese,
                                false
                              ])),
                                                                 */
                          elevation: 2.0,
                          fillColor: Colors.white,
                          child: Text(
                            "Liste",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          //padding: EdgeInsets.all(15.0),
                          shape: CircleBorder(),
                        ),
                        //width: 80.0,
                      ),
                    ],
                  ),
                ),

                Container(
                  child: Text("Milchprodukte",
                      textAlign: TextAlign.start,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ),

                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width / 12),
                        width: MediaQuery.of(context).size.width / 3,
                        //width: MediaQuery.of(context).size.width / 3,
                        child: DropdownButton<String>(
                          value: Strings.milchkategorie,
                          icon: Icon(Icons.arrow_drop_down),
                          iconSize: 20,
                          onChanged: (String newValue) {
                            setState(() {
                              Strings.milchkategorie = newValue;
                            });
                          },
                          items: <String>['Milch', 'Joghurt', 'Käse']
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 4,
                        //width: MediaQuery.of(context).size.width / 3,
                        child: Center(
                          child: DropdownButton<int>(
                            value: Strings.mengeneedmilch,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 20,
                            onChanged: (int newValue) {
                              setState(() {
                                Strings.mengeneedmilch = newValue;
                              });
                            },
                            items: <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                                .map<DropdownMenuItem<int>>((int value) {
                              return DropdownMenuItem<int>(
                                value: value,
                                child: Text("$value"),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width / 12,
                        ),
                        width: MediaQuery.of(context).size.width / 4,
                        //width: MediaQuery.of(context).size.width / 3,
                        child: RawMaterialButton(
                          onPressed: () {
                            setState(() {

                              //Abfrage - Wenn Liste leer -> Neuer Eintrag
                              if(Strings.itemsneed2.isEmpty){
                                Strings.itemsneed2.add([Strings.milchkategorie, Strings.mengeneedmilch, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                              }

                              //Falls if-Abfrage unzutreffend in diese Funktion springen
                              else{
                                flagOnList = false;

                                //For-Schleife, um die Einträge der Liste durchzulaufen
                                for (var i = 0; i < Strings.itemsneed2.length; i++) {
                                  //Ist die Eingabe aus dem Textfeld bereits in der Liste vorhanden, wird nur die Menge angepasst
                                  if (Strings.itemsneed2[i][0] == Strings.milchkategorie) {
                                    Strings.itemsneed2[i][1] = Strings.itemsneed2[i][1] + Strings.mengeneedmilch;
                                    flagOnList = true;
                                    break;
                                  }
                                }

                                  //Sicherstellen, dass ein Eintrag für die Texteingabe erstellt wird
                                  if (flagOnList == false) {
                                    Strings.itemsneed2.add([Strings.milchkategorie, Strings.mengeneedmilch, false]); //Eintragen des Inputs in die Map der Einkaufsliste
                                  }
                              }
                            });


                          },


                          /*
                          => setState(() => Strings.itemsneed2
                                  .add([
                                Strings.milchkategorie,
                                Strings.mengeneedmilch,
                                false
                              ])),
    */


                          elevation: 2.0,
                          fillColor: Colors.white,
                          child: Text(
                            "Liste",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          //padding: EdgeInsets.all(15.0),
                          shape: CircleBorder(),
                        ),
                        //width: 80.0,
                      ),
                    ],
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
