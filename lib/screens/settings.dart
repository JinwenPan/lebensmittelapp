import 'package:flutter/material.dart';
import 'package:prepareNcare/config/strings.dart';
import 'dart:math';

class Settings extends StatefulWidget {
  //static const String routeName = Strings.routeSeite4;

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  String productString = "Paprika";
  int mengeString = 1;
  String lagerortString = "Kühlschrank";
  String mindestmengeString = "1";
  String einheitString = "1 Stk.";
  String preisString = "1€";
  String mhdString = "3 Tage";


  @override
  Widget build(BuildContext context) {
    return _buildSettingsScreen();
  }

  Scaffold _buildSettingsScreen() {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //Mailadresse des Users
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.all(5.0),
                child: Text(
                  ("example.user@mail.com"),
                ),
              ),

              //Profilbild des Users
              Container(
                child: RawMaterialButton(
                  onPressed: () {
                    {
                      setState(() {});
                    }
                  },
                  elevation: 2.0,
                  fillColor: Colors.white,
                  child: Icon(Icons.face),
                  padding: EdgeInsets.all(15.0),
                  shape: CircleBorder(),
                ),
                width: 80.0,
              ),

              //Appversion
              Container(
                margin: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text("Premium"),
                    ),
                  ],
                ),
              ),

              //Definition Startseite
              Container(
                alignment: Alignment.centerLeft,

                margin: EdgeInsets.only(left: 20, right: 5, top: 20, bottom: 5),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text("Startseite     "),
                    ),
                    Container(
                      child: DropdownButton<String>(
                        value: Strings.startString,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 20,
                        onChanged: (String newValue) {
                          setState(() {
                            Strings.startString = newValue;
                          });
                        },
                        items: <String>[
                          'Analytics',
                          'Einkaufsliste',
                          'Vorrat',
                          'Planer'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    )
                  ],
                ),
              ),

              //MHD Alarm ein/aus
              Container(
                alignment: Alignment.centerLeft,

                margin: EdgeInsets.only(left: 20, right: 5, top: 5, bottom: 5),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text("MHD-Alarm      "),
                    ),
                    Container(
                      child: Switch(
                        value: Strings.isSwitched,
                        activeColor: Color.fromRGBO(43, 125, 50, 1),
                        onChanged: (value) {
                          setState(() {
                            Strings.isSwitched = value;
                            print(Strings.isSwitched);
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),

              //Freunde hinzufügen
              Container(
                alignment: Alignment.centerLeft,

                margin: EdgeInsets.only(left: 20, right: 5, top: 5, bottom: 5),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text("Freunde hinzufügen     "),
                    ),
                    Container(
                      child: RawMaterialButton(
                        onPressed: () {
                          {
                            setState(() {});
                          }
                        },
                        elevation: 2.0,
                        fillColor: Colors.white,
                        child: Icon(Icons.group_add),
                        padding: EdgeInsets.all(15.0),
                        shape: CircleBorder(),
                      ),
                      width: 80.0,
                    )
                  ],
                ),
              ),

              //Freundesliste
              Container(
                alignment: Alignment.centerLeft,

                margin: EdgeInsets.all(20.0),
                child: Row(
                 // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text("Freundesliste:"),
                    ),
                  ],
                ),
              ),

              //Liste der Freunde
              Container(
                margin: EdgeInsets.only(left: 20, right: 5, top: 5, bottom: 5),
                alignment: Alignment.centerLeft,
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Icon(Icons.delete),
                    ),
                    Container(
                      child: Text("     Sara"),
                    ),

                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 5, top: 5, bottom: 5),
                alignment: Alignment.centerLeft,
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Icon(Icons.delete),
                    ),
                    Container(
                      child: Text("     Leon"),
                    ),

                  ],
                ),
              ),

              Container(
                margin: EdgeInsets.all(20.0),
                child: RaisedButton(
                  onPressed: () {
                    {
                      setState(() {});
                    }
                  },
                  child: Text("Speichern"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
