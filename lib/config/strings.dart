import 'package:flutter_calendar_week/flutter_calendar_week.dart';
import 'package:flutter/material.dart';

class Strings {

  //Strings
  static const seite1 = 'Seite 1';
  static const seite2 = 'Seite 2';
  static const seite3 = 'Seite 3';
  static const seite4 = 'Seite 4';
  static String title = "MainPage";


  //Mengen
  static int paprika=7;
  static int butter=3;
  static int milk=1;
  static int cheese=11;
  static int sausage=4;
  static int applejuice=8;
  static int ice=24;
  static int icecreamchoco=3;
  static int icecreamstraw=6;
  static int toiletpaper=2;
  static int tomatoes=1;

  static int paprikatrash=0;
  static int buttertrash=0;
  static int milktrash=0;
  static int cheesetrash=0;
  static int sausagetrash=0;
  static int applejuicetrash=0;
  static int icetrash=0;
  static int icecreamchocotrash=0;
  static int icecreamstrawtrash=0;
  static int toiletpapertrash=0;
  static int tomatoestrash=0;

  static int paprikaused=0;
  static int butterused=0;
  static int milkused=0;
  static int cheeseused=0;
  static int sausageused=0;
  static int applejuiceused=0;
  static int iceused=0;
  static int icecreamchocoused=0;
  static int icecreamstrawused=0;
  static int toiletpaperused=0;
  static int tomatoesused=0;

  static double euro = 1.86;

  // Namen Rezepte
  static String nudelauflauf = 'Nudelauflauf';
  static String gnocchi = 'Gnocchi';
  static String fleischGemuesePfanne = 'Fleisch-Gemüse-Pfanne';
  static String hotdogs = 'Hot Dogs';

  //Bildpfad Rezepte
  static String nudelauflaufBild = 'lib/assets/pastabake.jpg';
  static String gnocchiBild = 'lib/assets/gnocchi.jpg';
  static String fleischGemuesePfanneBild = 'lib/assets/fleischgemuesepfanne.jpg';
  static String hotdogsBild = 'lib/assets/hotdogs.jpg';

  //Rezepte anzeigen
  static String vorschauBild = 'lib/assets/empty.png';
  static String titelRezept = '';
  static String zutat1 = '';
  static String zutat2 = '';
  static String zutat3 = '';
  static String mengeZutat1 = '';
  static String mengeZutat2 = '';
  static String mengeZutat3 = '';
  static String beschreibung = '';

  //Rezept auswählen
  static DateTime rezeptDatum;
  static String rezeptGewaehlt = '';

  //Rezepte Liste
  static List<DecorationItem> decorationItems = [];
  static int row = 31;
  static int col = 2;
  static var geplanteRezepte = List.generate(row, (i) => List(col));

  //Rezepte Zutaten+Beschreibung
  static int anzahlPersonen = 3;
  static int personenOld = 3;
    //Hot Dog
  static String zutatHotDog1 = 'Hot Dog Brötchen';
  static String zutatHotDog2 = 'Würstchen';
  static String zutatHotDog3 = 'Gurken';
  static String beschreibungHotDog = 'Brötchen und Würstchen erwärmen. Anschließend das Brötchen aufschneiden und das Würstchen hineinlegen. Zum Schluss noch mit Gurkenscheiben garnieren.';
    //Nudelauflauf
  static String zutatNudelauflauf1 = 'Nudeln';
  static String zutatNudelauflauf2 = 'Käse';
  static String zutatNudelauflauf3 = 'Tomaten';
  static String beschreibungNudelauflauf = 'Nudeln und Tomaten mit etwas Wasser kochen. Anschließend in eine Auflaufform geben und mit Käse bestreuen. Dann in den Ofen geben, bis der Käse geschmolzen ist.';
    //Gnocchi
  static String zutatGnocchi1 = 'Gnocchi';
  static String zutatGnocchi2 = 'Tomaten';
  static String zutatGnocchi3 = 'Käse';
  static String beschreibungGnocchi = 'Gnocchi in Wasser kochen. Anschließend mit Tomaten und Käse in eine Auflaufform geben. In den Backofen bis der Käse geschmolzen ist.';
    //FleischGemuesePfanne
  static String zutatFleischGemuesePfanne1 = 'Hähnchenfleisch';
  static String zutatFleischGemuesePfanne2 = 'Paprika';
  static String zutatFleischGemuesePfanne3 = 'Brokkoli';
  static String beschreibungFleischGemuesePfanne = 'Das Hähnchenfleisch in kleine Stücke schneiden. Anschließend das Fleisch mit der Paprika und dem Brokkoli in eine Pfanne geben. Erhitzen, bis das Fleisch leicht angebraten ist.';

  //Mengen Rezepte
  static int mengeZutatHotDog1 = 2;
  static int mengeZutatHotDog2 = 2;
  static int mengeZutatHotDog3 = 1;
  static int mengeZutatNudelauflauf1 = 1;
  static int mengeZutatNudelauflauf2 = 1;
  static int mengeZutatNudelauflauf3 = 1;
  static int mengeZutatGnocchi1 = 1;
  static int mengeZutatGnocchi2 = 2;
  static int mengeZutatGnocchi3 = 1;
  static int mengeZutatFleischGemuesePfanne1 = 2;
  static int mengeZutatFleischGemuesePfanne2 = 1;
  static int mengeZutatFleischGemuesePfanne3 = 3;

  //Status Zutaten für Rezepte filtern
  static bool kaesePlus = false;
  static bool kaeseMinus = false;
  static bool tomatenPlus = false;
  static bool tomatenMinus = false;
  static bool paprikaPlus = false;
  static bool paprikaMinus = false;

  //Farben für Filterbuttons regeln
  static Color kaesePlusColor = Colors.white;
  static Color kaeseMinusColor = Colors.white;
  static Color tomatenPlusColor = Colors.white;
  static Color tomatenMinusColor = Colors.white;
  static Color paprikaPlusColor = Colors.white;
  static Color paprikaMinusColor = Colors.white;

  //Rezepte filtern
  static bool gnocchiVisible = true;
  static bool nudelauflaufVisible = true;
  static bool fleischgemuesepfanneVisible = true;
  static bool hotdogsVisible = true;

  static bool addGroceryVisible = false;


  //Für Einkaufsliste
  //Liste zur Generierung der Einkaufsliste
  static Map<String, bool> itemsneed = { };
  static int rowGrocery = 0;
  static int colGrocery = 3;
  static var itemsneed2 = List.generate(rowGrocery, (i) => List(colGrocery), growable: true);

  static int mengeneedinput = 1;

  static var controller = TextEditingController();

  static String input = "";

  //Mengen Lebensmittel (Für Dropdown in Kategoriensuche)
  static int mengeneed = 1;
  static int mengeneedobst = 1;
  static int mengeneedgemuese = 1;
  static int mengeneedmilch = 1;
  //static int mengeneedgetraenke = 1;
  static int milkneed = 0;
  static int tomatoesneed = 0;

  //Namen Lebensmittel
  static String productmilk = "Milch";

  //Produktkategorien
  static String obstkategorie = "Apfel";
  static String gemuesekategorie = "Paprika";
  static String milchkategorie = "Milch";
  //static String getraenkekategorie = "Cola";

  // Routes
  static const String routeInitial = '/';

  static const String routeSeite1 = '/seite1';
  static const String routeSeite2 = '/seite2';
  static const String routeSeite3 = '/seite3';
  static const String routeSeite4 = '/seite4';

  static bool isSwitched = true;
  static String startString = "Vorrat";

}